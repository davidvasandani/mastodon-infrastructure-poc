version: "3.9"

services:
  postgres:
    # TODO: add the following lines to postgresql.conf:
    # shared_preload_libraries = 'pg_stat_statements'
    # pg_stat_statements.track = all
    container_name: postgres
    restart: always
    image: arm64v8/postgres:15-alpine
    shm_size: 256mb
    networks:
      - internal
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "postgres"]
    volumes:
      - pg_data:/var/lib/postgresql/data
      - ./configs/postgres/init.sql:/docker-entrypoint-initdb.d/init.sql
    env_file: .env
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.postgres
    labels:
      logging: promtail
      logging_jobname: containerlogs

  # TODO install custom root certificates
  caddy:
    container_name: caddy
    build: 
      context: docker
      dockerfile: Dockerfile.caddy
    restart: unless-stopped
    ports:
      - 80:80
      - 443:443
      - 2019:2019
    networks:
      - external
      - internal
    environment:
      - CADDY_INGRESS_NETWORKS=internal
      - CADDY_DOCKER_NO_SCOPE=true
      - CADDY_DOCKER_CADDYFILE_PATH=/data/caddy/files/Caddyfile
    volumes:
      - ./configs/caddy/certs:/data/caddy/pki/authorities/local/
      - ./configs/caddy:/data/caddy/files
      - /var/run/docker.sock:/var/run/docker.sock
      - caddy_data:/data
      - ./docker/php:/php-fpm-root/php-test
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.caddy
    labels:
      logging: promtail
      logging_jobname: containerlogs

  redis:
    container_name: redis
    restart: always
    image: arm64v8/redis:7-alpine
    networks:
      - internal
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
    volumes:
      - redis_data:/data
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.redis
    labels:
      logging: promtail
      logging_jobname: containerlogs

  elasticsearch:
    container_name: elasticsearch
    restart: always
    image: docker.elastic.co/elasticsearch/elasticsearch:8.5.2-arm64
    environment:
      - "ELASTIC_USERNAME=admin"
      - "ELASTIC_PASSWORD=changeme"
      - "ES_JAVA_OPTS=-Xms512m -Xmx512m -Des.enforce.bootstrap.checks=true"
      - "xpack.license.self_generated.type=basic"
      - "xpack.security.enabled=false"
      - "xpack.watcher.enabled=false"
      - "xpack.graph.enabled=false"
      - "xpack.ml.enabled=false"
      - "bootstrap.memory_lock=true"
      - "cluster.name=es-mastodon"
      - "discovery.type=single-node"
      - "thread_pool.write.queue_size=1000"
    networks:
      - internal
    ports:
      - 9200:9200
    healthcheck:
      # prettier-ignore
      test: ["CMD-SHELL", "curl --silent --fail localhost:9200/_cluster/health || exit 1"]
    volumes:
      - es_data:/usr/share/elasticsearch/data
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.elasticsearch
    labels:
      logging: promtail
      logging_jobname: containerlogs

  mastodon:
    build:
      context: docker
      dockerfile: Dockerfile.mastodon
    container_name: mastodon
    image: mastodon
    restart: always
    env_file: .env
    command: bash -c "rm -f /mastodon/tmp/pids/server.pid; bundle exec rake db:migrate; bundle exec rails server -b 0.0.0.0 -p 3000"
    # ports:
    #   - 3000:3000
    networks:
      - external
      - internal
    labels:
      caddy: dev.vasandani.me
      caddy.reverse_proxy: "{{ upstreams 3000 }}"
      caddy.tls: internal
      logging: promtail
      logging_jobname: containerlogs
    healthcheck:
      # prettier-ignore
      test: ['CMD-SHELL', 'wget -q --spider --proxy=off localhost:3000/health || exit 1']
    depends_on:
      postgres: { condition: service_healthy }
      redis: { condition: service_healthy }
      elasticsearch: { condition: service_healthy }
      mastodon-init: { condition: service_completed_successfully }
    volumes: &mastodon_volumes
      - mastodon_data:/opt/mastodon/public/system
      - ./configs/mastodon/development.rb:/opt/mastodon/config/environments/development.rb
      - ./configs/mastodon/credential_account_serializer.rb:/opt/mastodon/app/serializers/rest/credential_account_serializer.rb
      - ./configs/mastodon/post-init.sh:/tmp/post-init.sh
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.mastodon

  # https://github.com/docker/compose/issues/3270#issuecomment-1245819741
  mastodon-init:
    image: busybox:latest
    container_name: mastodon-init
    volumes:
      - mastodon_data:/opt/mastodon/public/system
    # https://github.com/mastodon/mastodon/blob/main/Dockerfile#L43-L56
    # https://serverfault.com/a/984599
    command: /bin/sh -c "touch /opt/mastodon/public/system/.initialized; chown -R 991:991 /opt/mastodon/public/system/"

  mastodon-shell:
    image: mastodon
    container_name: mastodon-shell
    restart: "no"
    env_file: .env
    environment:
      DEBUG: "true"
    networks:
      - internal
    volumes: *mastodon_volumes
    depends_on:
      mastodon: { condition: service_healthy }
    command: ["/tmp/post-init.sh"]
    labels:
      logging: promtail
      logging_jobname: containerlogs
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.mastodon-shell

  streaming:
    image: mastodon
    container_name: mastodon-streaming
    restart: always
    # build:
    #   context: docker
    #   dockerfile: Dockerfile.mastodon
    env_file: .env
    command: node ./streaming
    depends_on:
      postgres: { condition: service_started }
      redis: { condition: service_healthy }
    networks:
      - external
      - internal
    healthcheck:
      # prettier-ignore
      test: ['CMD-SHELL', 'wget -q --spider --proxy=off localhost:4000/api/v1/streaming/health || exit 1']
    ports:
      - 4000:4000
    labels:
      caddy: dev.vasandani.me
      caddy.handle_path: /api/v1/streaming/*
      caddy.handle_path.reverse_proxy: "{{ upstreams 4000 }}"
      caddy.tls: internal
      logging: promtail
      logging_jobname: containerlogs
    logging:
      driver: fluentd
      options:
        fluentd-address: 127.0.0.1:24224
        fluentd-async-connect: "true"
        tag: service.mastodon-streaming

  sidekiq:
    image: mastodon
    container_name: mastodon-sidekiq
    # build:
    #   context: docker
    #   dockerfile: Dockerfile.mastodon
    restart: always
    env_file: .env
    command: bundle exec sidekiq
    networks:
      - internal
    depends_on:
      postgres: { condition: service_started }
      redis: { condition: service_healthy }
      elasticsearch: { condition: service_healthy }
      mastodon-init:
        condition: service_completed_successfully
    volumes:
      - mastodon_data:/opt/mastodon/public/system
    healthcheck:
      test: ["CMD-SHELL", "ps aux | grep '[s]idekiq\ 6' || false"]

  sidekiq-web:
    container_name: sidekiq-web
    build:
      context: docker/sidekiq-webui
      dockerfile: Dockerfile.sidekiq-webui
    environment:
      REDIS_URL: redis://redis:6379
    command: rackup config.ru --host 0.0.0.0 --port 3030 --debug
    networks:
      - external
      - internal
    ports:
      - 3030:3030
    labels:
      caddy: dev.vasandani.me
      caddy.route: /sidekiq*
      caddy.route.reverse_proxy: "{{ upstreams 3030 }}"
      caddy.tls: internal

volumes:
  caddy_data:
  pg_data:
  redis_data:
  es_data:
  mastodon_data:

networks:
  internal:
    name: internal
    external: true
  external:
    name: external
    external: true
