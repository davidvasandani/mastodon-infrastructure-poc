# Mastodon Local Setup

## Dependencies

[Taskfile](https://taskfile.dev/installation/)  
Docker

## Setup

1. `task`

/ # ls -l /data/caddy/pki/authorities/local/
total 16
-rw-------    1 root     root           676 Jun 13 06:47 intermediate.crt
-rw-------    1 root     root           227 Jun 13 06:47 intermediate.key
-rw-------    1 root     root           631 Jun 13 06:47 root.crt
-rw-------    1 root     root           227 Jun 13 06:47 root.key

docker cp caddy:/data/caddy/pki/authorities/local/intermediate.key ./configs/caddy/

## URLS

| service    | url                               |
| ---------- | --------------------------------- |
| mastodon   | https://dev.vasandani.me/         |
| grafana    | https://grafana.dev.vasandani.me/ |
| mail       | https://dev.vasandani.me/mail/    |
| cloudbever | https://db.dev.vasandani.me/      |

## TODO

- add Prometheus as a data source
- switch RAILS_ENV to development
