#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail
[[ "${DEBUG:=false}" == 'true' ]] && set -o xtrace

bundle exec tootctl cache clear

bundle exec tootctl accounts create \
  david \
  --email 'david@vasandani.me' \
  --confirmed \
  --role Owner \
  --force

# bundle exec tootctl accounts modify \
#   david \
#   --reset-password

# curl -vvv -ks -X POST \
#   -d "client_name=OAuth2Test&redirect_uris=&scopes=write read&website=https://auth.dev.vasandani.me/" \
#   http://mastodon:3000/api/v1/apps |
#   jq '.'

# curl -X POST -sS \
#   https://remote.vasandani.me/api/v1/apps \
#   -F "client_name=OAuth2Test" \
#   -F "redirect_uris=https://auth.dev.vasandani.me/oauth-callback/" \
#   -F "scopes=read write follow" \
#   -F "website=https://auth.dev.vasandani.me/" | jq '.'
